<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','MainPageController@index');
Route::redirect('/home','admin/dashboard');
Route::get('masuk','Auth\LoginController@showLoginForm')->name('login');
Route::post('login','Auth\LoginController@login');
Route::post('logout','Auth\LoginController@logout')->name('logout');

Route::get('berita/{news}','ViewNewsController@show')->name('news.item.show');
Route::get('daftar-berita','ViewNewsController@index')->name('news.list');

Route::get('daftar-isu','ViewClarificationController@index')->name('clarification.list');
Route::get('isu/{clarification}','ViewClarificationController@show')->name('clarification.item.show');
Route::prefix('admin')->group(function(){
    Route::get('dashboard','DashboardController@profile')->name('admin.dashboard');
    Route::get('contact','DashboardController@contact')->name('admin.contact');
    Route::get('visi-misi','DashboardController@campaign')->name('admin.visi');
    Route::get('berita','DashboardController@news')->name('admin.news');
    Route::get('galeri','DashboardController@gallery')->name('admin.gallery');
    Route::get('lihat-berita/{news}','NewsController@show')->name('news.show');
    Route::get('daftar-klarifikasi','DashboardController@clarification')->name('admin.clarification');
    Route::get('klarifikasi/{clarification}','ClarificationController@show')->name('clarification.show');
    
    Route::post('add-image','GalleryController@storeImage');
    Route::post('update-contact','ContactController@update')->name('contact.update');
    Route::post('update-profile','ProfileController@update')->name('profile.update');
    Route::post('add-vision','CampaignController@storeVision');
    Route::post('add-mission','CampaignController@storeMission');
    Route::post('delete-mission/{mission}','CampaignController@deleteMission');
    Route::post('delete-vision/{vision}','CampaignController@deleteVision');
    Route::post('store-news','NewsController@store')->name('news.store');
    Route::post('update-news/{news}','NewsController@update')->name('news.update');
    Route::post('delete-news/{news}','NewsController@delete')->name('news.delete');
    Route::post('insert-image/{news}','NewsController@uploadAsset')->name('news.image');
    Route::post('delete-image/{media}','NewsController@deleteAsset')->name('news.image.delete');  
    Route::post('store-clarification','ClarificationController@store')->name('clarification.store');
    Route::post('clarification/{clarification}','ClarificationController@update')->name('clarification.update');
    Route::post('clarification/{clarification}/delete','ClarificationController@delete')->name('clarification.delete');
    
});

