<?php

use App\Vision;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class VisionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i = 0; $i < 5; $i++)
        {
            $data[$i] = [
                "content" => Faker::create()->text(100)
            ];
        }
        Vision::insert($data);
    }
}
