<?php

use App\Mission;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class MissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i = 0; $i < 5; $i++)
        {
            $data[$i] = [
                "content" => Faker::create()->text(100)
            ];
        }
        Mission::insert($data);
    }
}
