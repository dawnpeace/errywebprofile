<?php

use App\Contact;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class ContactSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(!Contact::count())
        {
            Contact::create([
                "instagram" => "sahabaterry",
                "facebook" => "erry.iriansyahst",
                "phone" => "+6280000000",
                "email" => "erryiriansyahst@gmail.com",
                "address" => "Pontianak, Kalimantan Barat"
            ]);
        }
    }
}
