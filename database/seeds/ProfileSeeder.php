<?php

use App\Profile;
use Illuminate\Database\Seeder;

class ProfileSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(!Profile::count())
        {
            Profile::create([
                "head" => "Putra Asli Daerah",
                "first_right_title" => "Wakil Ketua Bidang Pemuda dan Olahraga Partai Golongan Karya",
                "first_right_content" => "Periode 2017 - 2020",
                "second_right_title" => "Wakil Ketua HIPMI",
                "second_right_content" => "Periode 2019",
                "first_left_title" => "Alumnus Universitas Tanjungpura",
                "first_left_content" => "Strata I Teknik Universitas Tanjungpura.",
                "second_left_title" => "Ketua DPD KNPI",
                "second_left_content" => "Regional Kalimantan Barat Periode 2017 - 2020"
            ]);
        }
    }
}
