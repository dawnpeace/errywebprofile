<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('head');
            $table->string('first_right_title');
            $table->string('first_right_content');

            $table->string('second_right_title');
            $table->string('second_right_content');

            $table->string('first_left_title');
            $table->string('first_left_content');

            $table->string('second_left_title');
            $table->string('second_left_content');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profiles');
    }
}
