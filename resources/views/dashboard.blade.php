<!DOCTYPE HTML>
<html>

<head>
    <title>Sahabat Erry Iriansyah</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="/css/main.css" />
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
</head>

<body>

    <!-- Header -->
    <header id="header">
        <nav class="left">
            <a href="#menu"><span>Menu</span></a>
        </nav>
        <a href="{{url('/')}}" class="logo">Erry Iriansyah, S.T.</a>
    </header>

    <!-- Menu -->
    <nav id="menu">
        <ul class="links">
            <li><strong>Kunjungi Kami</strong></li>
            <li><a href="https://facebook.com/{{ $contact->facebook }}"><span class="icon fa-facebook"></span> Facebook</a></li>
            <li><a href="https://instagram.com/{{ $contact->instagram }}"><span class="icon fa-instagram"></span> Instagram</a></li>
            <li><a href="/daftar-berita"><span class="icon fa-newspaper-o"></span> Kabar Berita</a></li>
            <li><a href="/daftar-berita"><span class="icon fa-newspaper-o"></span> Klarifikasi Hoax</a></li>
        </ul>
    </nav>

    <!-- Banner -->
    <section id="banner">
        <div class="content">
            <h1 style="color:yellow;display:block;" class="">Bersama Membangun Kalimantan Barat</h1>
            <p style="font-weight:bold">Bersama #SahabatErryIriansyah</p>
            <ul class="actions">
                <li><a href="#footer" data-aos-easing="ease" class="button scrolly">Kontak Kami</a></li>
            </ul>
        </div>
    </section>

    <!-- One -->
    <section id="one" data-aos="fade-up" class="wrapper">
        <h1 style="text-align:center;font-weight:bold">{{$profile->head}}</h1>
        <div class="inner flex flex-3">
            <div class="flex-item left">
                <div class="w-100">
                    <h3>{{$profile->first_left_title}}</h3>
                    <p>{!! nl2br($profile->first_left_content) !!}</p>
                </div>
                <div class="w-100">
                    <h3>{{$profile->second_left_title}}</h3>
                    <p>{!! nl2br($profile->second_left_content) !!}</p>
                </div>
            </div>
            <div class="flex-item image fit round">
                <img style="object-fit:cover;max-height:15rem;object-position:top" src="/images/main.jpeg" alt="" />
            </div>
            <div class="flex-item right">
                <div class="w-100">
                    <h3>{{$profile->first_right_title}}</h3>
                    <p>{!!nl2br($profile->first_right_content)!!}</p>
                </div>
                <div class="w-100">
                    <h3>{{$profile->second_right_title}}</h3>
                    <p>{!! nl2br($profile->second_right_content) !!}</p>
                </div>
            </div>
        </div>
    </section>

    <!-- Two -->
    <section id="two" class="parallax center-flex">
        <div class="parallax">
            <div style="background-color:rgb(255,255,255,0.5);padding:2em;margin:2em">
                <div class="inner">
                    <h1 class="c-dark align-center">Visi dan Misi Bang Erry</h1>
                </div>
                <div class="inner flex flex-2" data-aos="fade-down">
                    <div class="flex-item">
                        <figure>
                            <h3 class="c-dark">Visi</h3>
                            @foreach($visions as $vision)
                            <blockquote style="font-weight:bold;padding-right:1em">
                                {{ $vision->content }}
                            </blockquote>
                            @endforeach
                        </figure>
                    </div>
                    <div class="flex-item">
                        <figure>
                            <h3 class="c-dark">Misi</h3>
                            @foreach($missions as $mission)
                            <blockquote style="font-weight:bold;padding-right:1em">
                                {{ $mission->content }}
                            </blockquote>
                            @endforeach
                        </figure>
                    </div>
                </div>
            </div>

        </div>
    </section>

    <!-- Three -->
    <section data-aos="fade-down" id="three" class="wrapper {{$news->isEmpty() ? "d-none" : ""}}">
        <h2 class="align-center">Berita Bang Erry</h2>
        <div class="inner flex flex-3">
            @foreach($news as $item)
            <div class="flex-item box">
                <div class="image fit">
                    <img src="{{$item->thumbnail()}}" alt="" />
                </div>
                <div class="content">
                    <h3>{{$item->title}}</h3>
                    <p class="align-right">
                        <a href="{{route('news.item.show',$item)}}">Baca Selengkapnya . .</a>
                    </p>
                </div>
            </div>
            @endforeach
        </div>
    </section>

    <section data-aos="fade-down" id="four" class="wrapper {{$clarifications->isEmpty() ? "d-none" : ""}}">
        <h2 class="align-center">Pelurusan Isu</h2>
        <div class="inner flex flex-3">
            @foreach($clarifications as $item)
            <div class="flex-item box">
                <div class="content">
                    <h3>{{$item->title}}</h3>
                    <p class="align-right">
                        <a href="{{route('clarification.item.show',$item)}}">Baca Selengkapnya . .</a>
                    </p>
                </div>
            </div>
            @endforeach
        </div>
    </section>

    <!-- Footer -->
    <footer id="footer">
        <div class="inner">
            <h2>Kontak #SahabatErryIriansyah</h2>
            <ul class="actions">
                <li><span class="icon fa-phone"></span> <a href="#">{{$contact->phone}}</a></li>
                <li><span class="icon fa-envelope"></span> <a href="#">{{$contact->email}}</a></li>
                <li><span class="icon fa-map-marker"></span> {{$contact->address}}</li>
                <li><span class="icon fa-instagram"></span> <a href="https://instagram.com/{{$contact->instagram}}">{{$contact->instagram}}</a></li>
                <li><span class="icon fa-facebook"></span> <a href="https://facebook.com/{{$contact->facebook}}">{{$contact->facebook}}</a></li>
            </ul>
        </div>
    </footer>

    <!-- Scripts -->
    <script src="/js/jquery.min.js"></script>
    <script src="/js/jquery.scrolly.min.js"></script>
    <script src="/js/skel.min.js"></script>
    <script src="/js/util.js"></script>
    <script src="/js/main.js"></script>
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <script>
        AOS.init();
    </script>
</body>

</html>