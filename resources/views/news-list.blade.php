<!DOCTYPE html>
<html lang="en">
<head>
    <title>Sahabat Erry Iriansyah</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="/css/main.css" />
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
</head>
<body>

        <!-- Header -->
        <header id="header">
            <nav class="left">
                <a href="#menu"><span>Menu</span></a>
            </nav>
            <a href="{{url('/')}}" class="logo">Erry Iriansyah, S.T.</a>
        </header>
    
        <!-- Menu -->
        <nav id="menu">
            <ul class="links">
                <li><strong>Kunjungi Kami</strong></li>
                <li><a href="index.html"><span class="icon fa-facebook"></span> Facebook</a></li>
                <li><a href="generic.html"><span class="icon fa-instagram"></span> Instagram</a></li>
                <li><a href="elements.html"><span class="icon fa-whatsapp"></span> Whatsapp</a></li>
            </ul>
        </nav>
    
        <!-- Banner -->
        <section data-aos="fade-down" id="three" class="wrapper">
            <div style="padding:2em;" class="inner box">
                <h3 class="align-center">{{$news->title}}</h3>
                <div>
                    {!! $news->content !!}
                </div>
            </div>
        </section>
        <!-- Footer -->
        <footer id="footer">
            <div class="inner">
                <h2>Kontak #SahabatErryIriansyah</h2>
                <ul class="actions">
                    <li><span class="icon fa-phone"></span> <a href="#">(000) 000-0000</a></li>
                    <li><span class="icon fa-envelope"></span> <a href="#">information@untitled.tld</a></li>
                    <li><span class="icon fa-map-marker"></span> Kalimantan Barat</li>
                    <li><span class="icon fa-instagram"></span> ErryIriansyah</li>
                    <li><span class="icon fa-facebook"></span> Erry Iriansyah</li>
                </ul>
            </div>
        </footer>
    
        <!-- Scripts -->
        <script src="/js/jquery.min.js"></script>
        <script src="/js/jquery.scrolly.min.js"></script>
        <script src="/js/skel.min.js"></script>
        <script src="/js/util.js"></script>
        <script src="/js/main.js"></script>
        <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
        <script>
            AOS.init();
        </script>
    </body>
</html>