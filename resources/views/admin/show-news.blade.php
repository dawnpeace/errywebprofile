@extends('admin.shared.index')

@section('content')
@if(Session::has('success'))
<div class="alert alert-success mb-3" role="alert">
    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
    {{Session::get('success')}}
</div>
@endif
    <div class="card">
        <div class="card-body">
            <h4 class="text-center">Berita Bang Erry</h4>
            <form method="POST" action="{{ route('news.update',$news) }}">
                @csrf
                <div class="form-group">
                    <h5>Judul</h5>
                    <input type="text" id="title" name="title" value="{{old('title') ?? $news->title}}" class="form-control {{$errors->has('title') ? 'is-invalid' : ''}}" />
                    @if ($errors->has('title'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('title') }}</strong>
                    </span>
                    @endif
                </div>
                
                <h5>Konten Berita</h5>
                <textarea name="content" id="editor" cols="30" rows="10">
                        {{$news->content}}
                </textarea>

                @if($errors->has('content'))
                <p class="text-danger">{{ $errors->first('content') }}</p>
                @endif

                <div class="text-right mt-3">
                    <button type="submit" class="btn btn-primary">Perbaharui</button>
                </div>
            </form>
            <hr>
            <div class="row mb-4">
                <h4 class="col-sm-12 text-center">Gallery Berita</h4>
                <div class="col-md-4 col-sm-12">
                    <form method="POST" action="{{ route('news.image',$news) }}" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group pl-2">
                            <label for="image">Unggah Gambar</label>
                            <input type="file" id="image" name="image" accept="image/png,image/jpeg,image/jpg" class="form-control-file" />
                        </div>
                        <small>Maksimal ukuran file 2048 Kb</small>
                        @if ($errors->has('image'))
                            <p class="text-danger"> {{ $errors->first('image') }} </p>
                        @endif
                        <div class="text-right">
                            <button type="submit" class="btn btn-sm btn-primary">Unggah Gambar</button>
                        </div>
                    </form>
                </div>
                <div class="col-md-8 col-sm-12">
                    <div class="row">
                        @foreach ($news->getMedia('images') as $image)
                            <div class="col-md-6 p-2">
                                <div class="p-2 border">
                                    <img class="img img-fluid" src="{{ $image->getUrl() }}" alt="">
                                </div>
                                <div class="text-right mt-3">
                                    <button data-url="{{ $image->getFullUrl() }}" class="btn btn-sm btn-view btn-outline-primary m-1">
                                        <i class="fa fa-eye"></i> Lihat Gambar
                                    </button>
                                    <button data-url="{{ route('news.image.delete',$image->id) }}" class="btn btn-sm btn-outline-danger btn-delete m-1">
                                        <i class="fa fa-trash"></i> Hapus Gambar
                                    </button>
                                    <button data-clipboard-text="{{ $image->getFullUrl() }}" class="btn btn-sm btn-outline-info btn-copy m-1">
                                        <i class="fa fa-link"></i> Copy Link
                                    </button>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>    
        </div>
    </div>
    <form method="POST" id="form-delete" action="">
        @csrf
    </form>
@endsection

@push('scripts')
<script src="{{asset('js/ckeditor/ckeditor.js')}}"></script>
<script src="{{asset('js/clipboard/clipboard.min.js')}}"></script>
<script>
    $(document).ready(function(){
        CKEDITOR.replace('editor',{
            removeButtons: 'Table,SpecialChar,Source'
        });
        clipboard = new ClipboardJS('.btn-copy');
        $('.btn-view').click(function(){
            window.open($(this).data('url'),'_blank').focus();
        });
        $('.btn-delete').click(function(){
            swal({
                title: "Apakah anda yakin ?",
                text: "Apakah anda yakin ingin menghapus ?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if(willDelete){
                    url = $(this).data('url');
                    $('#form-delete')
                        .attr('action',url)
                        .submit();
                }
            });
        });
    });
</script>
@endpush