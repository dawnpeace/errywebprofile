@extends('admin.shared.index')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <h2>Berita Oleh Bang Erry</h2>
                <form method="POST" action="{{ route('news.store') }}">
                    @csrf
                    <div class="form-group">
                        <label for="title">Judul Berita</label>
                        <div class="input-group">
                            <input type="text" id="title" name="title" value="{{old('title')}}" class="form-control {{$errors->has('title') ? 'is-invalid' : ''}}" />
                            <div class="input-group-append">
                                <button class="btn btn-primary">Tambah Berita</button>
                            </div>
                            @if ($errors->has('title'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('title') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                </form>

                <h2>Daftar Berita</h2>
                <ul class="list-group mb-3">
                    @foreach($news as $item)
                    <li class="list-group-item">
                        <h4 class="{{ blank($item->content) ? 'text-info' : '' }}">{{ $item->title }}</h4>
                        <p>
                            <i>Diunggah pada, {{$item->uploadedAt()}}</i>
                        </p>
                        <div class="text-right">
                            <button data-url="{{ route('news.delete',$item) }}" class="btn btn-outline-danger btn-delete">Hapus</button>
                            <a href="{{ route('news.show',$item) }}" class="btn btn-outline-primary">Edit</a>
                        </div>
                    </li>
                    @endforeach
                </ul>

                <div class="d-flex justify-content-center">
                    {{ $news->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
<form id="form-delete" method="POST" action="">
    @csrf
</form>
@endsection

@push('scripts')
<script>
    $(document).ready(function(){
        $('.btn-delete').click(function(){
            swal({
                title : "Hapus Berita !",
                text : "Anda yakin ? ",
                dangerMode : true,
                icon : 'warning',
                buttons : true,
            })
            .then((willDelete) => {
                if(willDelete){
                    url = $(this).data('url');
                    $('#form-delete')
                        .attr('action',url)
                        .submit();
                }
            });
        });
    });
</script>
@endpush