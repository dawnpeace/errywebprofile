@extends('admin.shared.index')
@section('content')
    <div class="card">
        <div class="card-body">
            <form action="{{route('clarification.update',$clarification)}}" method="POST">
                @csrf
                <div class="form-group">
                    <label for="title">Judul</label>
                    <input type="text" id="title" name="title" value="{{old('title') ?? $clarification->title}}" class="form-control {{$errors->has('title') ? 'is-invalid' : ''}}" />
                    @if ($errors->has('title'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('title') }}</strong>
                    </span>
                    @endif
                </div>

                <div class="form-group">
                    <textarea name="content" id="editor" cols="30" rows="10">{!! $clarification->content !!}</textarea>
                </div>
                <div class="text-right">
                    <button class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
@endsection

@push('scripts')
<script src="{{asset('js/ckeditor/ckeditor.js')}}"></script>
<script src="{{asset('js/clipboard/clipboard.min.js')}}"></script>
<script>
    $(document).ready(function(){
        CKEDITOR.replace('editor',{
            removeButtons: 'ImageButton,Image,Table,SpecialChar,Source'
        });
        clipboard = new ClipboardJS('.btn-copy');
        $('.btn-view').click(function(){
            window.open($(this).data('url'),'_blank').focus();
        });
        $('.btn-delete').click(function(){
            swal({
                title: "Apakah anda yakin ?",
                text: "Apakah anda yakin ingin menghapus ?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if(willDelete){
                    url = $(this).data('url');
                    $('#form-delete')
                        .attr('action',url)
                        .submit();
                }
            });
        });
    });
</script>
@endpush