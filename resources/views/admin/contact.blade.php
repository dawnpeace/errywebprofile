@extends('admin.shared.index')

@section('content')
<div class="row">
    <div class="col-md-12 p-3">
        <div class="card">
            <div class="card-body">
                <contact-component
                    :contact = "{{ json_encode($contact) }}"
                    submit_url = "{{ route('contact.update') }}"
                />
            </div>
        </div>
    </div>
</div>
@endsection