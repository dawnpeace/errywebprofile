@extends('admin.shared.index')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <gallery-component/>
            </div>
        </div>
    </div>
</div>

@endsection