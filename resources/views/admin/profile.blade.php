@extends('admin.shared.index')

@section('content')
<div class="row">
    <div class="col-md-12 p-3">
        <div class="card">
            <div class="card-body">
                <profile-component
                    :profile = "{{json_encode($profile)}}"
                    submit_url = "{{ route('profile.update') }}"
                />
            </div>
        </div>
    </div>
</div>
@endsection