<!DOCTYPE html>
<html lang="en">
<head>
    @include('admin.shared.head')
</head>
<body>
    <div id="wrapper" class="d-flex">
        <!-- Sidebar -->
        <div class="bg-light border-right" id="sidebar-wrapper">
            <div class="sidebar-heading">Panel Edit </div>
                <div class="list-group list-group-flush">
                    <a href="{{ route('admin.dashboard') }}" class="list-group-item list-group-item-action bg-light">Dashboard</a>
                    <a href="{{ route('admin.contact') }}" class="list-group-item list-group-item-action bg-light">Kontak</a>
                    <a href="{{ route('admin.visi') }}" class="list-group-item list-group-item-action bg-light">Visi - Misi</a>
                    <a href="{{ route('admin.news') }}" class="list-group-item list-group-item-action bg-light">Daftar Berita</a>
                    <a href="{{ route('admin.clarification') }}" class="list-group-item list-group-item-action bg-light">Pelurusan Isu</a>
                </div>
            </div>
            <!-- /#sidebar-wrapper -->

            <!-- Page Content -->
            <div id="page-content-wrapper">
                <nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom">
                    <button class="btn btn-primary" id="menu-toggle">
                        <i class="fa fa-bars"></i>
                    </button>

                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
                        <li id="logout" class="nav-item active">
                            <a class="nav-link" href="#">Logout <span class="sr-only">(current)</span></a>
                            <form id="form-logout" method="POST" action="{{route('logout')}}">
                                @csrf
                            </form>
                        </li>
                    </ul>
                    </div>
                </nav>

                <div class="container-fluid">
                    <div id="app" class="p-4">
                        @yield('content')
                    </div>
                </div>
            </div>
        <!-- /#page-content-wrapper -->

        </div>
    </div>
    <script src="{{asset('js/app.js')}}"></script>
    @stack('scripts')
    <script>
        $(document).ready(function(){
            $("#menu-toggle").click(function(e) {
                e.preventDefault();
                $("#wrapper").toggleClass("toggled");
            });

            $('li#logout').click(function(){
                swal({
                    title : 'Logout',
                    text : 'Apakah anda yakin ingin keluar ?',
                    dangerMode : true,
                    buttons : true,
                    icon : 'warning'
                })
                .then((willDelete) => {
                    if(willDelete){
                        $('#form-logout').submit();
                    }
                });
            });
        });
    </script>
    @yield('js')
</body>
</html>