@extends('admin.shared.index')

@section('content')
<div class="row">
    <div class="col-md-12 p-3">
        <div class="card">
            <div class="card-body">
                <campaign-component
                    :visions = "{{ json_encode($vision) }}"
                    :missions = "{{ json_encode($mission) }}"
                />
            </div>
        </div>
    </div>
</div>
@endsection