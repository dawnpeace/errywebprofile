@extends('clarification.base')
@section('content')
<section data-aos="fade-down" id="three" class="wrapper">
    <div style="padding:2em;" class="inner box">
        <h3 class="align-center">{{$clarification->title}}</h3>
        <div>
            {!! $clarification->content !!}
        </div>
    </div>
    <div class="align-center">
        <button id="news-more">Lihat Semua Isu</button>
    </div>
</section>
@endsection
@push('scripts')
    <script>
        $('#news-more').click(function(){
            window.location.replace('{{route('clarification.list')}}');
        });
    </script>
@endpush
