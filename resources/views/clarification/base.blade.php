<!DOCTYPE html>
<html lang="en">
<head>
    <title>Sahabat Erry Iriansyah</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="/css/main.css" />
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
</head>
<body>
    @stack('css')
        <!-- Header -->
        <header id="header">
            <nav class="left">
                <a href="#menu"><span>Menu</span></a>
            </nav>
            <a href="{{url('/')}}" class="logo">Erry Iriansyah, S.T.</a>
        </header>
    
        <!-- Menu -->
        <nav id="menu">
            <ul class="links">
                <li><strong>Kunjungi Kami</strong></li>
                <li><a href="https://facebook.com/{{ $contact->facebook }}"><span class="icon fa-facebook"></span> Facebook</a></li>
                <li><a href="https://instagram.com/{{ $contact->instagram }}"><span class="icon fa-instagram"></span> Instagram</a></li>
                <li><a href="{{route('news.list')}}"><span class="icon fa-newspaper-o"></span> Daftar Berita</a></li>
            </ul>
        </nav>
    
        <!-- Banner -->
        @yield('content')
        <!-- Footer -->
        <footer id="footer">
            <div class="inner">
                <h2>Kontak #SahabatErryIriansyah</h2>
                <ul class="actions">
                    <li><span class="icon fa-phone"></span> <a href="#">(000) 000-0000</a></li>
                    <li><span class="icon fa-envelope"></span> <a href="#">information@untitled.tld</a></li>
                    <li><span class="icon fa-map-marker"></span> Kalimantan Barat</li>
                    <li><span class="icon fa-instagram"></span> ErryIriansyah</li>
                    <li><span class="icon fa-facebook"></span> Erry Iriansyah</li>
                </ul>
            </div>
        </footer>
    
        <!-- Scripts -->
        <script src="/js/jquery.min.js"></script>
        <script src="/js/jquery.scrolly.min.js"></script>
        <script src="/js/skel.min.js"></script>
        <script src="/js/util.js"></script>
        <script src="/js/main.js"></script>
        <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
        <script>
            AOS.init();
        </script>
        @stack('scripts')
    </body>
</html>