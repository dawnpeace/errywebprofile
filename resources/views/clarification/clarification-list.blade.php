@extends('clarification.base')
@section('content')
<section data-aos="fade-down" id="three" class="wrapper">
    <h2 class="align-center">Pelurusan Isu</h2>
    <div class="inner">
        <div class="row">
            @foreach($clarifications as $item)
            <div class="6u$ 12u$(small)">
                <div class="box hoax">
                    <h3 class="hoax-title">{{$item->title}}</h3>
                    <p class="hoax-content">{{ $item->description() }}</p>
                    <p class="align-right">
                        <a class="hoax-link" href="{{route('clarification.item.show',$item)}}">Baca Selengkapnya . .</a>
                    </p>
                </div>
            </div>
            @endforeach
        </div>
    </div>
    {{$clarifications->links()}}
</section>
@endsection
@push('css')
    <style>
        .pagination {
            display: flex;
            justify-content: center;
            padding-left: 0;
            list-style: none;
            border-radius: 0.25rem;
        }

        .page-link {
            position: relative;
            display: block;
            padding: 0.5rem 0.75rem;
            margin-left: -1px;
            line-height: 1.25;
            color: #3490dc;
            background-color: #fff;
            border: 1px solid #dee2e6;
        }
        a.page-link{
            text-decoration: none;
        }
        .page-link:hover {
            z-index: 2;
            color: #1d68a7;
            text-decoration: none;
            background-color: #e9ecef;
            border-color: #dee2e6;
        }

        .page-link:focus {
            z-index: 2;
            outline: 0;
            box-shadow: 0 0 0 0.2rem rgba(52, 144, 220, 0.25);
        }

        .page-item:first-child .page-link {
            margin-left: 0;
            border-top-left-radius: 0.25rem;
            border-bottom-left-radius: 0.25rem;
        }

        .page-item:last-child .page-link {
            border-top-right-radius: 0.25rem;
            border-bottom-right-radius: 0.25rem;
        }

        .page-item.active .page-link {
            z-index: 1;
            color: #fff;
            background-color: #3490dc;
            border-color: #3490dc;
        }

        .page-item.disabled .page-link {
            color: #6c757d;
            pointer-events: none;
            cursor: auto;
            background-color: #fff;
            border-color: #dee2e6;
        }
    </style>
@endpush