<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class News extends Model implements HasMedia
{
    use HasMediaTrait;

    protected $fillable = ['title', 'content'];
    
    public function uploadedAt()
    {
        return Carbon::parse($this->created_at)->isoFormat('dddd DD-MM-YYYY');
    }

    public function thumbnail()
    {
        return blank($this->getFirstMediaUrl('images')) ? 'images/default.png' : $this->getFirstMediaUrl('images');
    }
}
