<?php

namespace App\Http\Controllers;

use App\Clarification;
use App\Contact;
use App\Mission;
use App\News;
use App\Profile;
use App\Vision;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function profile()
    {
        $profile = Profile::first();
        return view('admin.profile',compact('profile'));
    }

    public function contact()
    {
        $contact = Contact::first();
        return view('admin.contact',compact('contact'));
    }

    public function campaign()
    {
        $vision = Vision::all();
        $mission = Mission::all();
        return view('admin.campaign',compact(['vision','mission']));
    }

    public function news()
    {
        $news = News::orderBy('created_at','DESC')->paginate(4);
        return view('admin.news',compact('news'));
    }

    public function clarification()
    {
        $clarifications = Clarification::orderBy('created_at','DESC')->paginate(4);
        return view('admin.clarifications',compact('clarifications'));
    }

    public function gallery()
    {
        return view('admin.gallery');
    }

}
