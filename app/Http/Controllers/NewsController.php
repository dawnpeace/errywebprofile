<?php

namespace App\Http\Controllers;

use App\News;
use Illuminate\Http\Request;
use Spatie\MediaLibrary\Models\Media;

class NewsController extends Controller
{

    public function index()
    {
        return News::orderBy('created_at','DESC')->paginate(2);
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required|string'
        ]);

        $news = News::create([
            'title' => $request->title
        ]);

        return redirect()->back()->with('success', 'Berita berhasil ditambahkan');
    }

    public function show(News $news)
    {
        return view('admin.show-news',compact('news'));
    }
    
    public function update(News $news ,Request $request)
    {
        $request->validate([
            'title' => 'required|string',
            'content' =>'required|string'
        ]);

        $news->update([
            'title' => $request->title,
            'content' => $request->content
        ]);
        return redirect()->back()->with('success','Konten berhasil diperbaharui !');
    }

    public function delete(News $news)
    {
        $news->clearMediaCollection('images');
        $news->delete();
        return redirect()->back()->with('success','Berita berhasil dihapus');
    }

    public function uploadAsset(News $news, Request $request)
    {
        $request->validate([
            'image' => 'required|mimes:jpeg,jpg,png|max:2048'
        ]);

        $news
            ->addMediaFromRequest('image')
            ->toMediaCollection('images');

        return redirect()->back()->with('success','Gambar berhasil ditambahkan');
    }

    public function deleteAsset(Media $media)
    {
        $media->delete();
        return redirect()->back()->with('succcess','Gambar berhasil dihapus !');
    }
}
