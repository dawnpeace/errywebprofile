<?php

namespace App\Http\Controllers;

use App\Clarification;
use App\Contact;
use App\Mission;
use App\News;
use App\Profile;
use App\Vision;
use Illuminate\Http\Request;

class MainPageController extends Controller
{
    public function index()
    {
        $visions = Vision::all();
        $missions = Mission::all();
        $profile = Profile::first();
        $contact = Contact::first();
        $clarifications = Clarification::query()
            ->orderBy('created_at','DESC')
            ->whereNotNull('content')
            ->limit(3)
            ->get();

        $news = News::query()
            ->orderBy('created_at','DESC')
            ->whereNotNull('content')
            ->limit(3)
            ->with('media')
            ->get();
        
        return view('dashboard',compact(['news','visions','missions','profile','contact', 'clarifications']));
    }
}
