<?php

namespace App\Http\Controllers;

use App\Contact;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function update(Request $request)
    {
        $request->validate([
            'instagram' => 'required|string',
            'facebook' => 'required|string',
            'phone' => 'required|string',
            'address' => 'required|string',
            'email' => 'required|email'
        ]);

        if(Contact::count()){
            Contact::first()->update($request->all());
        } else {
            Contact::create($request->all());
        }
    }
}
