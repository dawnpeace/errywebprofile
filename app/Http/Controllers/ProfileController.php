<?php

namespace App\Http\Controllers;

use App\Profile;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function update(Request $request)
    {
        $request->validate([
            "head" => "required|string",
            "first_left_title" => "required|string",
            "first_left_content" => "required|string",
            "first_right_title" => "required|string",
            "first_right_content" => "required|string",
            "second_left_title" => "required|string",
            "second_left_content" => "required|string",
            "second_right_title" => "required|string",
            "second_right_content" => "required|string",
        ]);

        if(Profile::count()){
            Profile::first()->update($request->all());
        } else {
            Profile::create($request->all());
        }
    }
}
