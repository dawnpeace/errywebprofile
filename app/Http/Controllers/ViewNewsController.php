<?php

namespace App\Http\Controllers;

use App\Contact;
use App\News;
use Illuminate\Http\Request;

class ViewNewsController extends Controller
{
    public function show(News $news)
    {
        if(blank($news->content))
        {
            return abort(404);
        }
        $contact = Contact::first();
        return view('news.show-news',compact(['news', 'contact']));
    }

    public function index()
    {
        $news = News::query()
            ->whereNotNull('content')
            ->paginate(1);
        $contact = Contact::first();
        return view('news.news-list',compact(['news','contact']));
    }
}
