<?php

namespace App\Http\Controllers;

use App\Mission;
use App\Vision;
use Illuminate\Http\Request;

class CampaignController extends Controller
{
    public function storeMission(Request $request)
    {
        $request->validate([
            "mission" => "required|string"
        ]);
        $vision = Mission::create(["content" => $request->mission]);
        return response()->json([
            "mission" => $vision
        ],201);
    }

    public function storeVision(Request $request)
    {
        $request->validate([
            "vision" => "required|string"
        ]);
        $mission = Vision::create(["content" => $request->vision]);
        return response()->json([
            "vision" => $mission
        ],201);

    }

    public function deleteVision(Vision $vision)
    {
        $vision->delete();
    }

    public function deleteMission(Mission $mission)
    {
        $mission->delete();
    }
}
