<?php

namespace App\Http\Controllers;

use App\Clarification;
use App\Contact;
use Illuminate\Http\Request;

class ViewClarificationController extends Controller
{
    public function index()
    {
        $clarifications = Clarification::query()
            ->orderBy('created_at','DESC')
            ->paginate();
        $contact = Contact::first();
        return view('clarification.clarification-list',compact(['clarifications','contact']));
    }

    public function show(Clarification $clarification)
    {
        $contact = Contact::first();
        return view('clarification.show-clarification',compact(['clarification','contact']));
    }
}
