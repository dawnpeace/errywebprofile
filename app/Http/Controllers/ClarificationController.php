<?php

namespace App\Http\Controllers;

use App\Clarification;
use Illuminate\Http\Request;

class ClarificationController extends Controller
{
    public function index()
    {
        $clarifications = Clarification::all();
        return view('admin.clarification',compact('clarifications'));
    }

    public function show(Clarification $clarification)
    {
        return view('admin.show-clarification',compact('clarification'));
    }

    public function store(Request $request)
    {
        $data = $request->validate([
            "title" => 'required|string'
        ]);
        Clarification::create($data);
        return redirect()->back()->with('success','Konten berhasil ditambahkan');
    }

    public function delete(Clarification $clarification)
    {
        $clarification->delete();
        return redirect()->back()->with('success','Item berhasil dihapus');
    }

    public function update(Clarification $clarification, Request $request)
    {
        $data = $request->validate([
            "content" => 'required|string',
            "title" => 'required|string'
        ]);

        $clarification->update($data);

        return redirect()->route('clarification.list')->with('success','Konten berhasil diperbaharui');
    }
}
