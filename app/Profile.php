<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $fillable = [
        "head",
        "first_right_title",
        "first_right_content",
        "second_right_title",
        "second_right_content",
        "first_left_title",
        "first_left_content",
        "second_left_title",
        "second_left_content"
    ];
}
