<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Clarification extends Model
{
    protected $fillable = ["title", "content"];

    public function uploadedAt()
    {
        return Carbon::parse($this->created_at)->isoFormat('dddd DD-MM-YYYY');
    }

    public function description()
    {
        $striped = strip_tags($this->content);
        $words = explode(' ',$striped);
        return implode(" ",array_slice($words,0,12));
    }
}
